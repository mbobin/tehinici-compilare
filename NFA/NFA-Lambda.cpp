
// Bobin Marius
// Grupa 341.
// Semigrupa 1.

#include<iostream>
#include<fstream>
#include<string.h>
using namespace std;

class NFAL {

	int nr_stari, nr_stari_finale, *Stari_finale, cardinalul_alfabetului, nr_tranzitii, stare_curenta;

	char *Alfabet, lambda;

	struct tranz{
		int from, to;
		char simbol;} *Tranzitii;

	bool OK;

	void tranzitie(char *w, int stare_curenta, int lungime);

public:
	NFAL(const char *fisier){

		stare_curenta = 0;
		OK = false;

		fstream g(fisier,ios::in);

		g >> nr_stari;
		g >> nr_stari_finale;

		g >> lambda;

		// Citeste starile finale
		Stari_finale = new int[nr_stari_finale];
		for(int i=0; i < nr_stari_finale; i++)
			g >> Stari_finale[i];

		// Citeste alfabetul
		g >> cardinalul_alfabetului;
		Alfabet = new char[cardinalul_alfabetului];
		for(int i=0; i < cardinalul_alfabetului; i++)
			g >> Alfabet[i];

		// Citeste tranzitiile
		g >> nr_tranzitii; 
		Tranzitii = new tranz[nr_tranzitii];
		for(int i=0; i < nr_tranzitii; i++)
			g >> Tranzitii[i].from >> Tranzitii[i].simbol >> Tranzitii[i].to;

		g.close();
	}

	bool verifica(char *s);
	void reset_ok(){ OK = false; }
};

void NFAL::tranzitie(char *w, int stare_curenta, int lungime){

	// Daca am ajuns la lungimea cuvantului cautat,
	// inseamna ca s-au verificat toate literele
	// si daca starea e finala,
	// cuvantul este acceptat
	// si iese din functie.
	if(lungime == strlen(w)){		
		for(int i=0; i < nr_stari_finale; i++)
			if(Stari_finale[i] == stare_curenta){
				OK = true;
				return;
			}
			else {
				for(int j=0; j < nr_tranzitii; j++)
				if(Tranzitii[j].from == stare_curenta && Tranzitii[j].simbol == lambda){
						tranzitie(w, Tranzitii[j].to, lungime); 
					} 
				}
		}
		else
			// Daca starea nu este finala,
			// cuvantul nu este bun.

			if(lungime == strlen(w))
				OK = false;
			else
				// Porneste cautare recursiva din starea curenta
				// spre toate starile cu eticheta curenta sau lambda.
				for(int j=0; j < nr_tranzitii; j++){
					if(Tranzitii[j].from == stare_curenta && Tranzitii[j].simbol == w[lungime]){
						tranzitie(w, Tranzitii[j].to, lungime+1); // lungimea creste
					}
					// pastreaza lungimea si face o lambda-tranzitie
					if(Tranzitii[j].from == stare_curenta && Tranzitii[j].simbol == lambda){
						tranzitie(w, Tranzitii[j].to, lungime); 
					}
				}
	}

bool NFAL::verifica(char *w){

		// Apeleaza functia tranzitie cu cuvantul w,
		// starea initiala 0, si prima litera din cuvant.
		tranzitie(w, 0, 0);
		// Salveaza rezultatul functiei.
		// Daca cuvantul a fost acceptat sau nu.
		bool x = OK;
		// Reseteaza variabila OK pentru a verifica urmatorul cuvant.
		reset_ok(); 
		return x;
}


int main(){
	
	char w[1024];
	
	NFAL a("fisier.txt");
	
	while(true){
		cout << "Introduceti un cuvant:" << endl ; 
		cin >> w;

		a.verifica(w) == true ? cout << "  => Cuvant Acceptat!\n\n" : 	cout << "  => Cuvant Respins!\n\n";
	}
}