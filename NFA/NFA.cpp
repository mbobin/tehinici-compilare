
// Bobin Marius
// Grupa 141.
// Semigrupa 1.

#include<iostream>
#include<fstream>
#include<string.h>
using namespace std;

class NFA {

	int q, f, *F, v, d, sc;

	char *V;

	struct tranz{
		int a,b;
		char c;}*D;

	bool OK;

	void tranzitie(char *w, int sc, int lungime);

public:
	NFA(char *fisier){

		sc = 0;
		OK = false;

		fstream g(fisier,ios::in);

		g >> q;
		g >> f;

		F = new int[f];
		for(int i=0; i < f; i++)
		g >> F[i];

		g >> v;
		V = new char[v];
		for(int i=0; i < v; i++)
			g >> V[i];
		g >> d; 
		D = new tranz[d];
		for(int i=0; i < d; i++)
			g >> D[i].a >> D[i].c >> D[i].b;

		g.close();
	}

	bool decizie(char *s);
	void reset_ok(){ OK = false; }

};

void NFA::tranzitie(char *w, int sc, int lungime){

	// Daca am ajuns la lungimea cuvantului cautat,
	// inseamna ca s-au verificat toate literele
	// si daca starea e finala,
	// cuvantul este acceptat
	//	si iese din functie.
	if(lungime == strlen(w)){		
		for(int i=0; i < f; i++)
			if(F[i] == sc){
				OK = true;
				return;
			}
		}
		else
			// Daca starea nu este finala,
			// cuvantul nu este bun.

			if(lungime == strlen(w))
				OK = false;
			else
				// Porneste cautare recursiva
				// spre toate starile cu eticheta curenta.
				for(int j=0; j <= d; j++)
					if(D[j].a == sc && D[j].c == w[lungime])
						tranzitie(w, D[j].b, lungime+1);

	}

bool NFA::decizie(char *w){

		// Apeleaza functia tranzitie cu cuvantul w,
		// starea initiala 0, si prima litera din cuvant.
			tranzitie(w, 0, 0);
		// Salveaza rezultatul functiei.
		// Daca cuvantul a fost acceptat sau nu.
			bool x = OK;
		// Reseteaza variabila OK pentru a verifica urmatorul cuvant.
			reset_ok(); 
		// Intoarce rezultatul.
			return x;
}


//	Backtracking pentru generarea tuturor cuvintelor.

/*	 Codificarea literelor in stiva st[].

0 a
1 b
2 c
3 d

*/

int st[50], k, as, ev;

 void init(int k, int st[]){
	// Initializeaza varful stivei cu -1.
	st[k] = -1;
}

int succesor(int k, int st[]){
	
	// Verifica daca valoarea din varful stivei mai poate fi crescuta
	// si o creste in caz afirmativ.
	if(st[k] < 3){

		st[k]++;

		return 1;
	}
	else 
		return 0;
}

int valid(int k, int st[]){
	// Se genereaza permutari,
	// deci valorile din stiva
	// o sa fie bune intotdeauna.
	
	return 1;
}




int main(){
	
	NFA a("fisier.txt");
	
	cout << "Introduceti un cuvant" << endl ; 
	char w[50];
	cin >> w;

	if(a.decizie(w) == true)
		cout << "Cuvant Acceptat!" << endl;
	else 
		cout << "Cuvant Respins!" << endl;


	int lung_cuv = strlen(w);

	cout << "\nGenerez toate cuvintele de dimensiunea cuvantului verificat: " << lung_cuv << endl <<endl;
	
	// Rutina Backtrackingului
	k = 1;
	init(k,st);

	while(k != 0){
		do{
			 as=succesor(k,st);
			if(as)
				ev=valid(k,st);
		}
		while(!((as && ev) || (!as)));
		if(as) 
			// Testeaza existenta solutiei.
			if(k == lung_cuv){
		char cuv[50];
		// Construieste cuvantul dupa valorile din stiva.
		for(int i=0; i < k; i++)
			switch(st[i+1]){
				case 0:{ cuv[i] = 'a'; break;}
				case 1:{ cuv[i] = 'b'; break;}
				case 2:{ cuv[i] = 'c'; break;}
				case 3:{ cuv[i] = 'd'; break;}
		}
		// Marcheaza sfarsitul cuvantului.
		cuv[k] = '\0';
		// Daca cuvantul e acceptat,
		// il afiseaza.
		if(a.decizie(cuv) == true)
			cout << cuv << endl;
		
	}				
			else{
				k++;
				init(k,st);
			}
		else
			k--;
	}
	getchar();
	getchar();
}